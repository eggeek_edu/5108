#!/bin/bash
o="literal"
if [ $# -gt 0 ] && [ "$1" == "-i" ]
then
  o="identifier"
fi
# ./src/tokenize.py "$o" < out/nogoods/nogoods
# cat out/nogoods/nogoods | src/transaction.py out/"$o" "$o"
./src/tokenize.py "$o" < data/search3.txt
cat data/search3.txt | src/transaction.py out/"$o" "$o"
