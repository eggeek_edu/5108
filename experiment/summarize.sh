#!/bin/bash
o="literal"
if [ $# -gt 0 ] && [ "$1" == "-i" ]
then
  o="identifier"
fi

src/summarize.py out/res/"$o"_rules.txt rule out/summary/"$o"_rules.txt
src/summarize.py out/res/"$o"_itemset.txt itemset out/summary/"$o"_itemset.txt
# src/summarize.py out/complement/"$o"_transactions itemset out/summary/"$o"_complement_itemset.txt
