#!/bin/bash

o="literal"
if [ $# -gt 0 ] && [ "$1" == "-i" ]
then
  o="identifier"
fi

java -jar spmf.jar run AprioriClose out/transaction/"$o"_transactions out/res/"$o"_itemset.txt 0.50
# java -jar spmf.jar run FPGrowth_itemsets out/transaction/"$o"_transactions out/res/"$o"_itemset.txt 0.68
java -jar spmf.jar run Closed_association_rules out/transaction/"$o"_transactions out/res/"$o"_rules.txt 0.50 0.8
# java -jar spmf.jar run FPGrowth_association_rules out/transaction/"$o"_transactions out/res/"$o"_rules.txt 0.68 0.8

