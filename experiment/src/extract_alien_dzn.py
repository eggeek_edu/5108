#!/usr/bin/env python
import sys

def work(in_path, out_path):

  with open(in_path, 'r') as raw:
      n = int(raw.readline().strip())
      feels = []
      for i in range(n):
        feels.append(int(raw.readline().strip()))

  feels.sort()
  with open(out_path, 'w') as f:
    f.write('feels=[%s]\n' % (','.join([str(i) for i in feels])))

if __name__ == "__main__":
  work(*sys.argv[1:])
