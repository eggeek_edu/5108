#!/usr/bin/env python
import sys
import json
import traceback

root = "/Users/eggeek/Semester3/fit5108/experiment/out/nogoods/"

def extract_nogoods():
  while True:
    try:
      line = raw_input()
      nogood = line.split(',')[9]
      if nogood != 'nogood' and nogood.strip():
        print nogood.strip()
    except Exception as e:
      if type(e) == EOFError:
        sys.stderr.write("nogoods finished.\n")
      else:
        sys.stderr(traceback.format_exc())
      break

def extract_nogoods_reference():
  with open(root + "nogoods_ids", "w") as nogood_ids:
    with open(root + "nogoods_info", "w") as infos:
      with open(root + "ids", "w") as ids:
        while True:
          try:
            line = raw_input()
            elms = line.split(',')
            nid, nogood, info = elms[1].strip(), elms[9].strip(), ','.join(elms[14:])
            if nogood != 'nogood' and nogood:
              ids.write('%s\n' % nid)
              json_info = json.loads(info)
              nogood_ids.write('%s,%s\n' % (nid, nogood))
              reference = [str(i) for i in json_info.get('nogoods', [])]
              if len(reference):
                infos.write('%s,%s\n' % (nid, ','.join(reference)))
          except Exception as e:
            if type(e) == EOFError:
              sys.stderr.write("extract nogoods reference finished.\n")
            else:
              sys.stderr.write(traceback.format_exc())
            break

def extract_top_k_nogoods(top=50000):
  count = dict()

  sys.stderr.write("loading...\n")
  with open(root + "ids") as nids:
    for i in nids:
      count[i.strip()] = 0

  sys.stderr.write("counting...\n")
  with open(root + "nogoods_info", "r") as infos:
    for line in infos:
      elms = line.strip().split(',')
      nid = elms[0]
      for i in elms[1:]:
        count[i] = count.get(i, 0) + 1
  sys.stderr.write("sorting...\n")
  items = count.items()
  items.sort(key=lambda x: x[1], reverse=True)
  if len(items) <= top:
    threshold = items[-1][1]
  else:
    threshold = items[top][1]
  threshold = max(threshold, 3)
  sys.stderr.write("writing...\n")
  with open(root + "nogood_rank", "w") as f:
    for item in items:
      f.write("%s,%s\n" % (item[0], item[1]))
  with open(root + "nogoods", "w") as f:
    with open(root + "nogoods_ids", "r") as nogood_ids:
      for line in nogood_ids:
        nid, nogood = line.split(',')
        if count[nid.strip()] >= threshold:
          f.write('%s\n' % nogood.strip())
  sys.stderr.write("finished\n")

if __name__ == "__main__":
  extract_nogoods_reference()
  extract_top_k_nogoods()
