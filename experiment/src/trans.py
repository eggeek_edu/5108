#!/usr/bin/env python
import re
import traceback
import sys

def trans_summary(id_literal, p, out_path):

  def id_to_literal(match):
    return id_literal[match.group(0)]

  with open(out_path, "w") as f:
    while True:
      try:
        line = raw_input()
        line = p.sub(id_to_literal, line.strip())
        f.write("%s\n" % line)
      except Exception as e:
        if type(e) == EOFError:
          sys.stderr.write("translate summary finished.\n")
          break
        else:
          sys.stderr.write(traceback.format_exc())

def trans_mining(id_literal, p, out_path):

  def id_to_literal(match):
    return id_literal[match.group(0)]

  with open(out_path, "w") as f:
    while True:
      try:
        itemset = raw_input()
        partA, partB = itemset.split('#SUP:')
        partA = p.sub(id_to_literal, partA)
        f.write('%s #SUP: %s\n' % (partA.strip(), partB.strip()))
      except Exception as e:
        if type(e) == EOFError:
          sys.stderr.write("translate mining finished.\n")
          break
        else:
          sys.stderr.write(traceback.format_exc())

def translate(id_fname, opt, out_path):
  id_literal = dict()
  with open(id_fname, "r") as f:
    for line in f:
      lid, literal = line.split()
      id_literal[lid] = literal
  p = re.compile(r"\d+")
  if opt == "mining":
    trans_mining(id_literal, p, out_path)
  elif opt == "summary":
    trans_summary(id_literal, p, out_path)
  else:
    assert(False)

if __name__ == "__main__":
  translate(*sys.argv[1:])
