#!/usr/bin/env python
from collections import deque
import sys

root = "/Users/eggeek/Semester3/fit5108/experiment/out/summary/"

class Rule:

  org = set()
  imp = set()

  def __init__(self, line):
    rule, _ = line.split('#SUP')
    orgs, imps = map(lambda i: i.strip(), rule.strip().split("==>"))
    self.org = set(map(int, orgs.split()))
    self.imp = set(map(int, imps.split()))


def load_rules(fname):
  res = []
  with open(fname, "r") as f:
    for line in f:
      res.append(Rule(line))
  return res

def load_itemsets(fname):
  res = []
  with open(fname, "r") as f:
    for line in f:
      items = map(int, line.split("#SUP")[0].strip().split())
      res.append(set(items))
  return res

def clean(rules=[]):
  res = []
  n = len(rules)
  for i in range(n):
    s = rules[i]
    flag = False
    for j in range(n):
      if j == i:
        continue
      if s.issubset(rules[j]):
        flag = True
        break
    if not flag:
      res.append(s)
  return res

def expand(rules=[]):
  q = deque()
  for i in rules:
    new = i.org.union(i.imp)
    if new not in q:
      q.append(new)
  res = []
  while len(q) > 0:
    cur = q.popleft()
    flag = False
    for i in rules:
      if i.org not in cur:
        continue
      nxt = cur.union(i.imp)
      if nxt != cur and nxt not in res:
        q.append(nxt)
        flag = True
    if not flag:
      res.append(cur)
  res = clean(res)
  return res

def summarize_rules(rules_path, out_path):
  rules = load_rules(rules_path)
  exp_rules = expand(rules)
  with open(out_path, "w") as f:
    for rule in exp_rules:
      line = list(rule)
      line.sort()
      f.write("%s\n" % (' '.join([str(i) for i in line])))

def summarize_itemset(itemset_path, out_path):
  itemsets = load_itemsets(itemset_path)
  res = clean(itemsets)
  with open(out_path, "w") as f:
    for line in res:
      l = sorted(list(line))
      f.write("%s\n" % ' '.join([str(i) for i in l]))

def work(fname, opt="rule", out_path=None):
  if opt == "rule":
    summarize_rules(fname, out_path)
  elif opt == "itemset":
    summarize_itemset(fname, out_path)

if __name__ == "__main__":
  work(*sys.argv[1:])
