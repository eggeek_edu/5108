import sys

root = "/Users/eggeek/Semester3/fit5108/experiment/out/"

def load_summarized_itemset():
  sets = []
  with open(root + "summary/literal_itemset.txt", "r") as f:
    for line in f:
      sets.append(set([int(i) for i in line.strip().split()]))
  return sets

def extract_complement(itemsets, transaction_path, out_path):
  sys.stderr.write("extracting complement...\n")
  with open(transaction_path, "r") as f:
    with open(out_path, "w") as out:
      for line in f:
        s = set([int(i) for i in line.strip().split()])
        sub = set()
        for itemset in itemsets:
          if itemset.issubset(s):
            sub = sub.union(itemset)
        if len(sub):
          comp = s - sub
          out_line = [i for i in comp]
          out_line.sort()
          out.write('%s\n' % ' '.join([str(i) for i in out_line]))

def describe(file_path):
  sys.stderr.write("extracting statistic info...\n")
  content = []
  with open(file_path, "r") as f:
    for line in f:
      content.append((line, len(line.split())))
  content.sort(key=lambda x: x[1])
  sys.stderr.write("writing rank info...\n")
  with open(file_path + "_rank", "w") as f:
    for i in content:
      f.write('%s' % i[0])


def work(transaction_path, out_path):
  itemsets = load_summarized_itemset()
  extract_complement(itemsets, transaction_path, out_path)
  describe(out_path)

if __name__ == "__main__":
  work(*sys.argv[1:])
