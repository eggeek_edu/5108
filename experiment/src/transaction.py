#!/usr/bin/env python
import traceback
import re
import sys

root = "/Users/eggeek/Semester3/fit5108/experiment/out/transaction/"

def identifier_transaction(id_fname):
  literal_id = dict()
  with open(id_fname, 'r') as f:
    for i in f:
      lid, literal = i.split()
      literal_id[literal] = lid
  p = re.compile('|'.join(map(re.escape, literal_id.keys())))
  subr = re.compile(r'[><=]+(:?(:?\d+)|(:?true)|(:?false))+')

  def litera_to_id(match):
    return literal_id[match.group(0)]

  with open(root + "identifier_transactions", "w") as f:
    while True:
      try:
        clause = subr.sub('', raw_input())
        # line = list(set([i for i in p.sub(litera_to_id, clause).split()]))
        line = list(set([int(i) for i in p.sub(litera_to_id, clause).split()]))
        line.sort()
        f.write("%s\n" % (' '.join([str(i) for i in line])))

      except Exception as e:
        if type(e) == EOFError:
          sys.stderr.write("identifier transaction finished.\n")
        else:
          sys.stderr.write(traceback.format_exc())
        break

def literal_transaction(id_fname):
  literal_id = dict()
  with open(id_fname, 'r') as f:
    for i in f:
      lid, literal = i.split()
      literal_id[literal] = lid
  p = re.compile('|'.join(map(re.escape, literal_id.keys())))

  def litera_to_id(match):
    return literal_id[match.group(0)]

  with open(root + "literal_transactions", "w") as f:
    while True:
      try:
        clause = raw_input()
        # line = list(set([i for i in p.sub(litera_to_id, clause.strip()).split()]))
        line = list(set([int(i) for i in p.sub(litera_to_id, clause.strip()).split()]))
        line.sort()
        f.write("%s\n" % (' '.join([str(i) for i in line])))
      except Exception as e:
        if type(e) == EOFError:
          sys.stderr.write("literal transaction finished.\n")
        else:
          sys.stderr.write(traceback.format_exc())
        break

def transaction(fname, opt='identifier'):
  if opt == 'identifier':
    identifier_transaction(fname)
  elif opt == 'literal':
    literal_transaction(fname)
  else:
    assert(False)

if __name__ == "__main__":
  transaction(*sys.argv[1:])
