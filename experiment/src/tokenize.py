#!/usr/bin/env python
import sys
import traceback
import re

root = "/Users/eggeek/Semester3/fit5108/experiment/out/"

def identifier_tokenize():
  p = re.compile(r'[^<=>]+')
  ids = set()
  idList = []
  with open(root + "identifier", "w") as f:
    while True:
      try:
        line = raw_input()
        ws = line.split()
        for w in ws:
          find = p.findall(w)
          if len(find) > 1:
            assert(len(find) == 2)
            if find[0] not in ids:
              ids.add(find[0])
              idList.append(find[0])
      except Exception as e:
        if type(e) == EOFError:
          sys.stderr.write("identifier tokenize finished.\n")
        else:
          sys.stderr.write(traceback.format_exc())
        break
    cnt = 1
    assert(len(ids) == len(idList))
    assert(ids == set(idList))
    for i in idList:
      f.write('%d %s\n' % (cnt, i))
      cnt += 1

def literal_tokenize():
  ids = set()
  idList = []
  with open(root + "literal", "w") as f:
    while True:
      try:
        line = raw_input()
        ws = line.split()
        for w in ws:
          if w not in ids:
            ids.add(w)
            idList.append(w)
      except Exception as e:
        if type(e) == EOFError:
          sys.stderr.write("literal tokenize finished.\n")
        else:
          sys.stderr.write(traceback.format_exc())
        break
    cnt = 1
    assert(len(ids) == len(idList))
    assert(ids == set(idList))
    for i in idList:
      f.write('%d %s\n' % (cnt, i))
      cnt += 1

def tokenize(opt='identifier'):
  if opt == 'identifier':
    identifier_tokenize()
  elif opt == 'literal':
    literal_tokenize()
  else:
    assert(False)

if __name__ == "__main__":
  tokenize(*sys.argv[1:])
