#!/bin/bash
o="literal"
if [ $# -gt 0 ] && [ "$1" == "-i" ]
then
  o="identifier"
fi

cat out/res/"$o"_rules.txt | src/trans.py out/"$o" mining out/res/"$o"_cp_rules.txt
cat out/summary/"$o"_rules.txt | src/trans.py out/"$o" summary out/summary/"$o"_cp_rules.txt
cat out/res/"$o"_itemset.txt | src/trans.py out/"$o" mining out/res/"$o"_cp_itemset.txt
cat out/summary/"$o"_itemset.txt | src/trans.py out/"$o" summary out/summary/"$o"_cp_itemset.txt
cat out/complement/"$o"_transactions_rank | src/trans.py out/"$o" summary out/complement/"$o"_cp_transactions_rank
