#!/bin/bash
o="literal"
if [ $# -gt 0 ] && [ "$1" == "-i" ]
then
  o="identifier"
fi

python src/complement.py out/transaction/"$o"_transactions out/complement/"$o"_transactions
